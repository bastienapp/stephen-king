import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coverUrl',
  standalone: true
})
export class CoverUrlPipe implements PipeTransform {

  transform(coverIds: number[]): string {
    if (coverIds && coverIds.length > 0 && coverIds[0] != -1) {
      const coverId = coverIds[0];
      // https://covers.openlibrary.org/b/id/416165161-M.jpg
      return "https://covers.openlibrary.org/b/id/" + coverId + "-M.jpg";
    } else {
      return "https://placehold.co/200x300";
    }
  }

}
