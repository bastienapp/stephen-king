import { Pipe, PipeTransform } from '@angular/core';
import { Book } from './Book';

@Pipe({
  name: 'bookFilter',
  standalone: true
})
export class BookFilterPipe implements PipeTransform {

  transform(bookList: Book[], doFilter: boolean): Book[] {
    if (doFilter) {
      return bookList.filter((book) => book.title.charAt(0).toLocaleLowerCase() == "a");
    } else {
      return bookList;
    }
  }

}
