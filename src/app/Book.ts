export type Book = {
  key: string,
  title: string,
  covers: number[], // et pas [number] !
  description : string | { value: string }
}
