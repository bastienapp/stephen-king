import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookApiService } from '../bookapi.service';
import { Book } from '../Book';
import { FormatDescriptionPipe } from '../pipe/format-description.pipe';

@Component({
  selector: 'app-book-details',
  standalone: true,
  imports: [FormatDescriptionPipe],
  templateUrl: './book-details.component.html',
  styleUrl: './book-details.component.css'
})
export class BookDetailsComponent {

  key: string | null = null;
  bookDetails: Book | null = null;

  constructor(
    private bookApiServiceInjected : BookApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    // console.log('methode synchrone', this.route.snapshot.params['bookId']);

    this.route.paramMap.subscribe(params => {
      // c'est identifiant va me permettre de charger les détails du livre correspondant
      this.key = params.get('bookId');

      if (this.key !== null) {
        this.bookApiServiceInjected.getBookByKey(this.key)
          .then((response) => this.bookDetails = response);
      }

    });
  }
}
