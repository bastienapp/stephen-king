class Hero {

  // propriétés
  /*name: string;
  damage: number;

  // constructeur qui hydrate les propriétés de la classe
  // en clair : quand je crée une nouvelle instance (un nouveau héro dans ce cas), je vais lui donner des valeurs pour chacunes de ses propriétés
  constructor(nameValue: string, damageValue: number) {
    this.name = nameValue;
    this.damage = damageValue;
  }*/

  constructor(public name: string, public damage: number) {}
}

const michel: Hero = new Hero("Michel", 12);
console.log(michel);
