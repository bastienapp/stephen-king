import { Injectable } from '@angular/core';
import { Book } from './Book';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';

type ResponseApi = {
  entries: Book[]
}

@Injectable({
  providedIn: 'root'
})
export class BookApiService {

  constructor(private http: HttpClient) { }

  getBookList(): Observable<Book[]> {
    return this.http.get<ResponseApi>('https://openlibrary.org/authors/OL2162284A/works.json')
      .pipe(
        map((response) => response.entries)
      )
  }

  // chaque un livre grace à son identifiant
  getBookByKey(bookKey: string): Promise<Book> {
    return fetch('https://openlibrary.org' + bookKey + '.json')
      .then((response) => response.json())
      .then((response: Book) => response);
  }
}
