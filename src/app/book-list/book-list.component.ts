import { Component } from '@angular/core';
import { BookApiService } from '../bookapi.service';
import { Book } from '../Book';
import { RouterLink } from '@angular/router';
import { CommonModule } from '@angular/common';
import { BookFilterPipe } from '../book-filter.pipe';
import { CoverUrlPipe } from '../pipe/cover-url.pipe';

@Component({
  selector: 'app-book-list',
  standalone: true,
  imports: [RouterLink, CommonModule, BookFilterPipe, CoverUrlPipe],
  templateUrl: './book-list.component.html',
  styleUrl: './book-list.component.css'
})
export class BookListComponent {
  bookList: Book[] = []
  applyFilter: boolean = false;

  // @Inject, inject() ou injection par constructeur
  /*bookApiService: BookApiService;

  constructor(bookApiInjected: BookApiService) {
    this.bookApiService = bookApiInjected;
  }*/
  // c'est Angular qui injecte (qui donne) une instance du service lorsqu'il va créer un BookListComponent
  constructor(private bookApiService: BookApiService) {}

  ngOnInit() {
    // ici j'ai besoin du service pour récupérer la liste des livres
    this.bookApiService.getBookList()
      .subscribe((response) => this.bookList = response);

    // on peut aussi faire comme ça
    // const result = await this.bookApiService.getBookList();
  }

  toggleFilter() {
    this.applyFilter = !this.applyFilter;
  }
}
